
## Notes from conference of without bots at mozilla

[Link](https://onlinexperiences.com/scripts/Server.nxp?LASCmd=AI:1;F:SF!42000&EventKey=236480)

Error management vs error handling
Error handling: move the error to some place you can process them
                this is done by the Result type
Error management: what are the error that a program can throw
                  how do you make error from one library understandable for
                  another
Failure focus is on error management
Error type creation for display and debug and backtrace in the std::Error type
is really tedious. A lot of boilerplate and traits to add. Also no support for
additional context related to an error.
error_chain tried to solve that that creates new types of error from existing
one. Also based on macro so big learning curve but error are pervasive.
Encourage to create one error type for the whole crate because of macros.

Failure introduce a new Fail trait. You can derive the trait to create new
Fail trait and an Error struct that all Fail traits can be cast to.

```rust
#[macro_use] extern crate failure;

use std::io;

use failure::{Backtrace, Fail};

#[derive(Debug, Fail)]
enum MyError {
    #[fail(display = "unknown error")]
    UnknownError(Backtrace),
    #[fail(display = "IO error {}", _0)]
    IoError(#[cause] io::Error),
}

// You can also convert existing error using a Fail impl
impl<F: Fail> From<F> for failure::Error {
    _
}
```

The Error type contains reference to the base Fail type and a backtrace
Anyway, it will always be coerced to a std::Error so you can use lib
that use failure::Error as if they implement the base std::Err
Conversely, there is a compat layer that implement blanket implementation
for all Fail traits.
