
#[macro_use] extern crate slog;
#[macro_use] extern crate serde;
use slog::{Drain, Logger};
extern crate slog_term;


use std::fs;
use std::path;
use std::io;
use std::num;
use std::collections::HashMap;
use reqwest;

use snafu::{Snafu, ResultExt, Backtrace, ErrorCompat, ensure};

#[derive(Debug, Snafu)]
enum CliError {
    #[snafu(display("Could not open file {}: {}", filename.display(), source))]
    OpenConfig { filename: path::PathBuf, source: std::io::Error },
    #[snafu(display("Could not save config {}: {}", filename.display(), source))]
    ReadConfig { filename: path::PathBuf, source: std::num::ParseIntError },
    #[snafu(display("Network error {:#?}", source.url()))]
    NetworkError { source: reqwest::Error, backtrace: Backtrace }
}

type Result<T, E = CliError> = std::result::Result<T, E>;

fn open_and_parse_file(file_name: &str) -> Result<i32, CliError> {
    let contents = fs::read_to_string(&file_name).context(OpenConfig { filename: file_name })?;
    let num: i32 = contents.trim().parse().context(ReadConfig {filename: file_name})?;
    Ok(num)
}

fn make_request() -> Result<HashMap<String, String>, CliError> {
    let res = reqwest::get("http://localhost:8081").context(NetworkError {})?;
    res.json().context(NetworkError {})?
}

fn main() -> Result<(), CliError> {
    let plain = slog_term::PlainSyncDecorator::new(std::io::stdout());
    let logger = Logger::root(
        slog_term::FullFormat::new(plain)
        .build().fuse(), o!()
    );
    let res = make_request()?;
    info!(logger, "{:#?}", res);
    Ok(())
}
